package com.gildedtros;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GildedTrosTest {

    @Test
    void normal_items_decrease(){
        Item[] items = new Item[]{
                new Item("foo", 10, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(9, items[0].sellIn);
        assertEquals(9, items[0].quality);

    }

    @Test
    void normal_items_decrease_expired(){
        Item[] items = new Item[]{
                new Item("foo", 0, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(-1, items[0].sellIn);
        assertEquals(8, items[0].quality);
    }

    @Test
    void normal_items_decrease_expired_negative_quality(){
        Item[] items = new Item[]{
                new Item("foo", 0, 0)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(-1, items[0].sellIn);
        assertEquals(0, items[0].quality);
    }



    @Test
    void good_wine_increases_in_quality_when_not_expired(){
        Item[] items = new Item[]{
                new Item("Good Wine", 10, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(9, items[0].sellIn);
        assertEquals(11, items[0].quality);
    }

    @Test
    void good_wine_increases_in_quality_twice_when_expired(){
        Item[] items = new Item[]{
                new Item("Good Wine", 0, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(-1, items[0].sellIn);
        assertEquals(12, items[0].quality);
    }

    @Test
    void good_wine_quality_is_never_more_than_50(){
        Item[] items = new Item[]{
                new Item("Good Wine", 0, 50)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(-1, items[0].sellIn);
        assertEquals(50, items[0].quality);
    }
    @Test
    void b_dawg_keychain_quality_never_lowers(){
        Item[] items = new Item[]{
                new Item("B-DAWG Keychain", 10, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(10, items[0].sellIn);
        assertEquals(10, items[0].quality);
    }
    @Test
    void b_dawg_keychain_quality_never_lowers_expired(){
        Item[] items = new Item[]{
                new Item("B-DAWG Keychain", -10, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(-10, items[0].sellIn);
        assertEquals(10, items[0].quality);
    }

    @Test
    void backstage_passes_increase_quality_more_than_10_SellIn(){
        Item[] items = new Item[]{
                new Item("Backstage passes for Re:Factor", 20, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(19, items[0].sellIn);
        assertEquals(11, items[0].quality);
    }

    @Test
    void backstage_passes_increase_quality_less_than_10_and_more_than_5_SellIn(){
        Item[] items = new Item[]{
                new Item("Backstage passes for Re:Factor", 10, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(9, items[0].sellIn);
        assertEquals(12, items[0].quality);
    }

    @Test
    void backstage_passes_increase_quality_less_than_5_and_more_than_0_SellIn(){
        Item[] items = new Item[]{
                new Item("Backstage passes for Re:Factor", 5, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(4, items[0].sellIn);
        assertEquals(13, items[0].quality);
    }

    @Test
    void backstage_passes_different_names(){
        Item[] items = new Item[]{
                new Item("Backstage passes for Re:Factor", 5, 10),
                new Item("Backstage passes for HAXX", 5, 12)

        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(4, items[0].sellIn);
        assertEquals(13, items[0].quality);
        assertEquals(4, items[1].sellIn);
        assertEquals(15, items[1].quality);
    }

    @Test
    void backstage_passes_lose_all_quality_when_expired(){
        Item[] items = new Item[]{
                new Item("Backstage passes for Re:Factor", 0, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(-1, items[0].sellIn);
        assertEquals(0, items[0].quality);
    }

    @Test
    void smelly_items_lose_quality_twice_as_fast(){
        Item[] items = new Item[]{
                new Item("Duplicate Code", 10, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(9, items[0].sellIn);
        assertEquals(8, items[0].quality);
    }

    @Test
    void smelly_items_lose_quality_twice_as_fast_expired(){
        Item[] items = new Item[]{
                new Item("Duplicate Code", 0, 10)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(-1, items[0].sellIn);
        assertEquals(6, items[0].quality);
    }

    @Test
    void smelly_items_quality_is_never_negative(){
        Item[] items = new Item[]{
                new Item("Duplicate Code", 0, 2)
        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(-1, items[0].sellIn);
        assertEquals(0, items[0].quality);
    }

    @Test
    void smelly_items_different_names(){
        Item[] items = new Item[]{
                new Item("Duplicate Code", 10, 10),
                new Item("Long Methods", 10, 8),
                new Item("Ugly Variable Names", 10, 6)

        };
        GildedTros app = new GildedTros(items);
        app.updateItems();
        assertEquals(9, items[0].sellIn);
        assertEquals(8, items[0].quality);

        assertEquals(9, items[1].sellIn);
        assertEquals(6, items[1].quality);

        assertEquals(9, items[2].sellIn);
        assertEquals(4, items[2].quality);
    }
}
