package com.gildedtros.templates;

import com.gildedtros.Item;

public class ItemTemplate {
    Item item;

    public ItemTemplate(Item item) {
        this.item = item;
    }

    public void updateQuality() {
        lowerQuality();
    }

    public void updateSellIn() {
        item.sellIn = item.sellIn - 1;
    }

    public void handleExpired() {
        lowerQuality();
    }

    protected void increaseQuality() {
        if (item.quality < 50) {
            item.quality = item.quality + 1;
        }
    }

    protected void lowerQuality() {
        if (item.quality > 0) {
            item.quality = item.quality - 1;
        }
    }
}
