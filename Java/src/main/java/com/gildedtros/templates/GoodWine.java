package com.gildedtros.templates;

import com.gildedtros.Item;

public class GoodWine extends ItemTemplate {
    public GoodWine(Item item) {
        super(item);
    }

    @Override
    public void updateQuality() {
        increaseQuality();
    }

    @Override
    public void handleExpired() {
        increaseQuality();
    }
}
