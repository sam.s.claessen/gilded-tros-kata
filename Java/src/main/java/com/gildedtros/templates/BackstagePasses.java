package com.gildedtros.templates;

import com.gildedtros.Item;

public class BackstagePasses extends ItemTemplate {
    public BackstagePasses(Item item) {
        super(item);
    }

    @Override
    public void updateQuality() {
        increaseQuality();
        if (item.sellIn < 11) {
            increaseQuality();
        }
        if (item.sellIn < 6) {
            increaseQuality();
        }
    }

    @Override
    public void handleExpired() {
        item.quality = 0;
    }
}
