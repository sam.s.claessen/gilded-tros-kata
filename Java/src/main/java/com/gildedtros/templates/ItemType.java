package com.gildedtros.templates;

import java.util.Arrays;
import java.util.function.Predicate;

public enum ItemType {

    NORMAL(new String[]{}),
    GOOD_WINE(new String []{"Good Wine"}),
    KEYCHAIN(new String []{"B-DAWG Keychain"}),
    BACKSTAGE_PASS(new String []{"Backstage passes for Re:Factor","Backstage passes for HAXX"}),
    SMELLY_ITEM(new String[]{"Duplicate Code", "Long Methods", "Ugly Variable Names"});
    private String[] names;

    ItemType(String[] names) {
        this.names = names;
    }

    public static ItemType nameToType(String name) {
        return Arrays.stream(
                ItemType.values()).filter(
                        (type)-> Arrays.stream(type.names).anyMatch(Predicate.isEqual(name)))
                .findFirst().orElse(ItemType.NORMAL);
    }

}
