package com.gildedtros.templates;

import com.gildedtros.Item;

public class ItemTemplateFactory {
    public static ItemTemplate createItemTemplate(Item item) {
        ItemTemplate itemTemplate;
        ItemType itemType = ItemType.nameToType(item.name);
        switch (itemType) {
            case GOOD_WINE:
                itemTemplate = new GoodWine(item);
                break;
            case KEYCHAIN:
                itemTemplate = new Keychain(item);
                break;
            case BACKSTAGE_PASS:
                itemTemplate = new BackstagePasses(item);
                break;
            case SMELLY_ITEM:
                itemTemplate = new SmellyItem(item);
                break;
            default:
                itemTemplate = new ItemTemplate(item);
                break;
        }
        return itemTemplate;
    }
}
