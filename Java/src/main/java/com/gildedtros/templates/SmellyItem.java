package com.gildedtros.templates;

import com.gildedtros.Item;

public class SmellyItem extends ItemTemplate{
    public SmellyItem(Item item) {
        super(item);
    }

    @Override
    protected void lowerQuality() {
        this.item.quality = Math.max(0,item.quality-2);
    }
}
