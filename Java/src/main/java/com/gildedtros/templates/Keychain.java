package com.gildedtros.templates;

import com.gildedtros.Item;

public class Keychain extends ItemTemplate {
    public Keychain(Item item) {
        super(item);
    }

    @Override
    public void updateQuality() {

    }

    @Override
    public void updateSellIn() {

    }

    @Override
    public void handleExpired() {

    }
}
