package com.gildedtros;

import com.gildedtros.templates.*;

class GildedTros {
    Item[] items;

    public GildedTros(Item[] items) {
        this.items = items;
    }

    public void updateItems() {
        for (Item item : items) {
            ItemTemplate itemTemplate = ItemTemplateFactory.createItemTemplate(item);

            itemTemplate.updateQuality();

            itemTemplate.updateSellIn();

            if (item.sellIn < 0) {
                itemTemplate.handleExpired();
            }
        }
    }

}