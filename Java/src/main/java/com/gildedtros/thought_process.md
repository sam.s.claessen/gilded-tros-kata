
After having a first look at the existing code, it's not ideal to make the changes in the requirements concerning "smelly" items.
So I'll be looking to refactor the code to a more modular design, so it'll be easier to build on and add the new type of items
To make sure the existing cases will still work, I'll start by writing tests covering all the different scenario's 
and regularly execute them while making the changes.

### Following scenario's will need to be tested:
- normal items
  - SellIn reduces by 1
  - Quality reduces by 1
  - Quality reduces by 2 after SellIn date is <= 0
  - Quality is never negative (< 0)

- "Good wine"
  - Quality increases by 1 when SellIn > 0
  - Quality increases by 2, when SellIn < 0
  - Quality is never > 50

- "B-DAWG Keychain"
  - Doesn't lower quality

- "Backstage passes"
  - Increases quality by 1 as SellIn > 10
  - Increases quality by 2 as SellIn > 5 and SellIn < 10
  - Increases quality by 3 as SellIn > 3 and SellIn < 5
  - Quality is 0 when SellIn < 0

### Things I've noticed after writing the tests:
- The names are case-sensitive, initially tested with "Good wine" instead of "Good Wine"
- The condition for "Good Wine" increasing more when 'expired' (SellIn < 0) wasn't described in the requirements, so I had to find that out while testing and adjust
- The SellIn not changing for "B-DAWG Keychain" wasn't described in the requirements


The next steps I'll be thinking about is how to make the code more readable and after that introduce more modular design, most likely with subclasses and polymorphism.

### Things I want to improve:
- get rid of the deep nesting in the if-statements
  - get rid of negative logic
  - isolate code based on function
  - swap to using switch statements per type
- introduce enums to handle the typings
- get rid of code duplication
- combine the Backstage passes logic -> maybe use a contains operator instead (low priority, does change the current implementation slightly, probably not going to do it in this exercise)



### Actual changes:
- change for loop to for each loop to get rid of item[i]
- rename updateQuality to updateItems since it does more than just updating quality
- introduce methodes updateQuality, updateSellIn and handleExpired to isolate parts of the logic


- updateQuality:
  - get rid of negative logic
  - seperate out "Good Wine" and "Backstage passes"
  - get rid of duplicate code, by introducing increaseQuality method
  - "Backstage passes" : remove nested if, since we're checking quality<50 in the subconditions anyway
  - replace original one with increaseQuality again
  - "B-DAWG Keychain": little bit of a dubious scenario for me
    - on one hand I don't want negative logic
    - on the other hand I don't want 'return' statements in an empty if statement
    - Here I chose to use the empty statement, considering this will end up being a "null object" pattern, when I introduce subclasses later


- updateSellIn
  - Same logic as above, get rid of the negative logic, but leave the empty if for "B-DAWG keychain"


- handleExpired
  - get rid of negative logic
  - move the SellIn < 0 outside the method again, since that's the expired cases
  - replace 
```agsl
    item.quality = item.quality - item.quality;
```
  with
```agsl
    item.quality = 0;
```
- Move the "B-DAWG Keychain" check and introduce lowerQuality method


### Introducing Subclasses
Since changing the item class itself isn't an option, I will make a wrapper around it and use that as the base "Normal iten"
I'll move all the logic concerning this item to that class and call the methods on it instead.

- Pass itemTemplate to all the methods, 
- Use ide refactoring tool to move them to itemTemplate
- remove item from the method signatures, since it's accessible to itemTemplate already
- move itemTemplate to its own package and reconsider accessibility of methods
- start making subclasses for every type
- changed if statement in creation to switch
- have some fun with enums and lambdas :)

### Smelly items

After all the refactoring adding a new type of item is just a case of making a new subclass and then overriding the 
correct methods according to the new logic

- make tests
  - Quality degrades twice as fast
    - Quality decreases by 2 when SellIn > 0
    - Quality decreases by 4 when SellIn < 0
    - Quality is never negative (< 0)
    - check different names
- create a subclass
- add it to the enum
- override the lowerQuality method
- change the base updateQuality method to use lowerQuality (oversight in the earlier refactoring)